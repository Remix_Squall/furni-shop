<%@page language="java" contentType="text/html"%>
<%
	String base = (String) application.getAttribute("base");
	String imageURL = (String) application.getAttribute("imageURL");
%>
<div class="header">
	<div class="titulo">
		<h1>Furni Shop</h1>
		<h2>El hogar que siempre imaginaste en Internet</h2>
	</div>
	<div class="cesta">
		<table>
			<tr>
				<td><a href="<%=base%>?action=mostrarCarrito">Mostrar
						Carrito</a></td>
				<td><a href="<%=base%>?action=mostrarCarrito"><img
						src="<%=imageURL%>cesta.png" border="0" /> </a></td>
			</tr>

		</table>
	</div>
</div>