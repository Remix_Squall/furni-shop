<%@page language="java" contentType="text/html"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="furni.shop.beans.Muebles"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<%
	String base = (String) application.getAttribute("base");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Resultados de B�squeda</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/inicio.css"
	type="text/css" />
</head>
<body>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<%
		String cadena = request.getParameter("cadena");
		if (cadena != null && !cadena.trim().equals("")) {
	%>
	<div class="contenido">
		<h2>Resultados de la b�squeda</h2>
		<table>
			<tr>
				<th>Mueble</th>
				<th>Precio</th>
				<th>Detalles</th>
			</tr>
			<%
				ArrayList muebles = dataManager.getResultadosBusqueda(cadena);
					Iterator iterator = muebles.iterator();
					while (iterator.hasNext()) {
						Muebles mueble = (Muebles) iterator.next();
						int pId = mueble.getId_mueble();
			%>
			<tr>
				<td><%=mueble.getNombre()%></td>
				<td><%=mueble.getPrecio()%>&#8364;</td>
				<td><a class="enlace"
					href="<%=base%>?action=detallesMueble&id_mueble=<%=pId%>">
						Detalles</a></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<%
		} else {
	%><div class="contenido"><div class="error">�Cadena de b�squeda err�nea!</div></div>
	<%
		}
	%>
	<div class="footer">Furni Shop � 2014. Creado por Luis Romero
		Moreno para IES Ram�n del Valle-Incl�n</div>
</body>
</html>
