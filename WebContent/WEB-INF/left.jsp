<%@page language="java" contentType="text/html"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%
	String base = (String) application.getAttribute("base");
%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<div class="navegador">
	<div class="buscador">
		<h3>B�squeda r�pida</h3>
		<h4>Mueble / Categor�a:</h4>
		<form style="border: 0px solid; padding: 0; margin: 0;">
			<input type="hidden" name="action" value="buscar" /> <input
				id="text" type="text" name="cadena" size="15" /> <input id="submit"
				type="submit" value="Buscar" />
		</form>
	</div>
	<div class="categorias">
		<h3>Categor�as</h3><ul>
		<%
			Hashtable categorias = dataManager.getCategorias();
			Enumeration idsCategorias = categorias.keys();
			while (idsCategorias.hasMoreElements()) {
				Object idCategoria = idsCategorias.nextElement();
				out.println("<li><a href=" + base
						+ "?action=seleccionarCatalogo&id="
						+ idCategoria.toString() + ">"
						+ categorias.get(idCategoria) + "</a></li>");
			}
		%>
		</ul>
	</div>
	<jsp:include page="login.jsp"></jsp:include>
</div>
