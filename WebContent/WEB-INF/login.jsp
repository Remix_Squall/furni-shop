<%@page language="java" import="java.io.*,java.util.*"
	contentType="text/html"%>
<%
	String base = (String) application.getAttribute("base");
%>
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:setProperty name="usuario" property="*" />

<%
	if (usuario.getId_usuario() == null) {
		usuario.setId_usuario("");
		usuario.setContras("");
		usuario.setCiudad("");
		usuario.setCP("-1");
		usuario.setDir("");
		usuario.setEmail("");
		usuario.setNombre("");
		usuario.setProvincia("");
		usuario.setTarjeta("-1");
	}
	if (!dataManager.getLogged(usuario.getId_usuario(),
			usuario.getContras())) {
%>

<div class="login">
	<h3>Login</h3>
	<form method="post" style="border: 0px solid; padding: 0; margin: 0;">
		<input type="hidden" name="action" value="login" />
		<h4>
			Usuario: <input type="text" name="id_usuario" size="15"
				value="<%usuario.getId_usuario();%>" />
		</h4>
		<h4>
			Password: <input type="password" name="contras" size="15"
				value="<%usuario.getContras();%>" />
		</h4>
		<%
			if (!usuario.getId_usuario().equals("")
						|| !usuario.getContras().equals("")) {
					usuario.setId_usuario("");
					usuario.setContras("");
		%>
		<div class="problema">Usuario/Password incorrectos</div>
		<%
			session.setAttribute("error", true);
				}
		%>
		<input type="submit" value="Login">
	</form>
	<form>
		<input type="hidden" name="action" value="registro" /> <input
			type="submit" value="Registrarse" />
	</form>
</div>
<%
	} else {
%>
<div class="login">
	<h3>Bienvenido</h3>
	<h4>
		Hola de nuevo,<br>
		<%=dataManager.getUsuario(usuario.getId_usuario()).getNombre()%></h4>
	<form>
		<input type="hidden" name="action" value="cerrarSesion" /> <input
			type="submit" value="Cerrar sesión">
	</form>
	<%
		if (dataManager.isUserAdmin(usuario.getId_usuario(),
					usuario.getContras())) {
	%>
	<form>
		<input type="hidden" name="action" value="admin" /> <input
			type="submit" value="Administración">
	</form>
	<%
		}
	%>
</div>
<%
	}
%>