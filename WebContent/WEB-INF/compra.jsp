<%@page language="java" contentType="text/html"%>
<%@page import="java.util.Hashtable"%>
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<jsp:useBean id="logueado" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:setProperty name="usuario" property="*" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Finalizar compra</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/inicio.css"
	type="text/css" />
</head>
<body>
	<%
		session.setAttribute("pag", "compra.jsp");
	%>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="contenido">
		<%
			String imageURL = (String) application.getAttribute("imageURL");
			Hashtable cesta = (Hashtable) session.getAttribute("cesta");
			if (cesta != null && !cesta.isEmpty()) {
				if (usuario.getId_usuario() == null) {
					usuario.setId_usuario("");
					usuario.setContras("");
					usuario.setCiudad("");
					usuario.setCP("-1");
					usuario.setDir("");
					usuario.setEmail("");
					usuario.setNombre("");
					usuario.setProvincia("");
					usuario.setTarjeta("-1");
				}
				if (dataManager.getLogged(usuario.getId_usuario(),
						usuario.getContras())) {
					logueado = dataManager.getUsuario(usuario.getId_usuario());
		%>
		<h2>Confirmar compra</h2>
		<form>
			<input type="hidden" name="action" value="confirmarPedido" />
			<p>
				Usted es
				<%=logueado.getNombre()%>
				y se le va a enviar el pedido a la siguiente direcci�n:
			</p>
			<div class="direccion">
				<%=logueado.getDir()%><br>
				<%=logueado.getCiudad()%>
				(<%=logueado.getProvincia()%>)<br> CP
				<%=logueado.getCP()%>
			</div>
			<p>Pulsando el siguiente bot�n usted confirmar� la compra y se la
				enviaremos a su domicilio.</p>
			<input type="submit" value="Confirmar" />
		</form>
		<%
			}
				if (!dataManager.getLogged(usuario.getId_usuario(),
						usuario.getContras())) {
		%>
		<h2>Autentificaci�n de Usuario</h2>
		<h3>Necesaria para la confirmaci�n de la compra</h3>
		<form method="post" style="border: 0px solid; padding: 0; margin: 0;">
			<input type="hidden" name="action" value="login2" />
			<h4>
				Usuario: <input type="text" name="id_usuario" size="15"
					value="<%usuario.getId_usuario();%>" />
			</h4>
			<h4>
				Password: <input type="password" name="contras" size="15"
					value="<%usuario.getContras();%>" />
			</h4>
			<%
				if (session.getAttribute("error") != null)
							if ((Boolean) session.getAttribute("error")) {
			%>
			<div class="problema">Usuario/Password incorrectos</div>
			<%
					session.removeAttribute("error");
				}
			%>
			<input type="submit" value="Login">
		</form>
		<form>
			<input type="hidden" name="action" value="registro" /> <input
				type="submit" value="Registrarse" />
		</form>
		<%
			}
			} else {
		%><div class="error">
			ERROR: �No puede comprar con un carrito de la compra vac�o!<br>
			<img src="<%=imageURL%>advertencia.png" />
		</div>
		<%
			}
		%>
	</div>
	<div class="footer">Furni Shop � 2014. Creado por Luis Romero
		Moreno para IES Ram�n del Valle-Incl�n</div>
</body>
</html>
