<%@page language="java" contentType="text/html"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="furni.shop.beans.Muebles"%>
<%@page import="furni.shop.beans.Pedidos"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<%
	String base = (String) application.getAttribute("base");
	session.setAttribute("pag", "cesta.jsp");
	Hashtable<String, Pedidos> cesta = (Hashtable) session
			.getAttribute("cesta");
	if (cesta == null) {
		cesta = new Hashtable<String, Pedidos>(10);
	}
	String action = request.getParameter("action");
	if (action != null && action.equals("addProducto")) {
		try {
			String id_mueble = request.getParameter("id_mueble");
			Muebles mueble = dataManager.getDetallesMueble(id_mueble);
			if (mueble != null) {
				Pedidos pedido = new Pedidos(mueble, "1");
				cesta.remove(id_mueble);
				cesta.put(id_mueble, pedido);
				session.setAttribute("cesta", cesta);
			}
		} catch (Exception e) {
			out.println("Error al a�adir el mueble seleccionado al carrito de la compra!");
		}
	}
	if (action != null && action.equals("actualizarProducto")) {
		try {
			String id_mueble = request.getParameter("id_mueble");
			String cantidad = request.getParameter("cantidad");
			Pedidos pedido = (Pedidos) cesta.get(id_mueble);
			if (pedido != null) {
				pedido.setCantidad(cantidad);
			}
		} catch (Exception e) {
			out.println("Error actualizando el carrito de la compra!");
		}
	}
	if (action != null && action.equals("borrarProducto")) {
		try {
			String id_mueble = request.getParameter("id_mueble");
			cesta.remove(id_mueble);
		} catch (Exception e) {
			out.println("�Error borrando el producto seleccionado del carrito de la compra!");
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Carrito de la Compra</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/inicio.css" type="text/css" />
</head>
<body>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<%
		if (!cesta.isEmpty()) {
	%>
	<div class="contenido">
		<h2>Carrito de la Compra</h2>
		<table>
			<tr>
				<th>Nombre</th>
				<th>Precio</th>
				<th>Cantidad</th>
				<th>Subtotal</th>
				<th>Borrar</th>
			</tr>
			<%
				Enumeration enumList = cesta.elements();
					double precioPedido = 0;
					double precioTotal = 0;
					while (enumList.hasMoreElements()) {
						Pedidos pedido = (Pedidos) enumList.nextElement();
						precioPedido = Math.round(Integer.parseInt(pedido
								.getCantidad()) * pedido.getPrecio() * 100.) / 100.;
						precioTotal += precioPedido;
			%>
			<tr>
				<td><%=pedido.getNombre()%></td>
				<td><%=Math.rint(pedido.getPrecio() * 100) / 100%>&#8364;</td>
				<td><form>
						<input type="hidden" name="action" value="actualizarProducto" />
						<input type="hidden" name="id_mueble"
							value="<%=pedido.getId_mueble()%>" /> <input type="text"
							size="2" name="cantidad" value="<%=pedido.getCantidad()%>" /> <input
							type="submit" value="Actualizar" />
					</form></td>
				<td><%=precioPedido%>&#8364;</td>
				<td><form>
						<input type="hidden" name="action" value="borrarProducto" /> <input
							type="hidden" name="id_mueble" value="<%=pedido.getId_mueble()%>" />
						<input type="submit" value="Borrar" />
					</form></td>
			</tr>
			<%
				}
			%>
			<tr>
				<td colspan="5" id="total" class="total">Total: <%=Math.rint(precioTotal * 100) / 100%>&#8364;</td>
				<td class="total">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><a class="enlace"
					href="<%=base%>?action=comprar">Comprar</a></td>
			</tr>
		</table>
	</div>
	<%
		} else {
	%><p class="contenido">El Carrito de la Compra est� vac�o.</p>
	<%
		}
	%>
	<div class="footer">Furni Shop � 2014. Creado por Luis Romero
		Moreno para IES Ram�n del Valle-Incl�n</div>
</body>
</html>
