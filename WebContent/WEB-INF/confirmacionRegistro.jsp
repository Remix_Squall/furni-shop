<%@ page language="java" import="java.io.*,java.util.*"
	contentType="text/html"%>
<jsp:useBean id="registrado" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"></jsp:useBean>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:setProperty name="registrado" property="*" />
<%
int CP, tarjeta;
try {
	CP = Integer.parseInt(registrado.getCP());
} catch (NumberFormatException e) {
	CP = -1;
	session.setAttribute("error", true);
	session.setAttribute("problema", 1);
}
registrado.setCP(String.valueOf(CP));
try {
	tarjeta = Integer.parseInt(registrado.getTarjeta());
} catch (NumberFormatException e) {
	tarjeta = -1;
	session.setAttribute("error", true);
	session.setAttribute("problema", 1);
}
registrado.setTarjeta(String.valueOf(tarjeta));
if(dataManager.isVacio(registrado)) {
	session.setAttribute("error", true);
	session.setAttribute("problema", 0);
}
if(dataManager.userExists(registrado.getId_usuario())) {
	session.setAttribute("error", true);
	session.setAttribute("problema", 2);
}
if(session.getAttribute("error") != null) {
	if((Boolean)session.getAttribute("error")) 
		pageContext.forward("registro.jsp");
}
if(session.getAttribute("error") == null) {
	dataManager.registroUsuario(registrado);
	usuario.setId_usuario(registrado.getId_usuario());
	usuario.setContras(registrado.getContras());
	session.removeAttribute("registrado");
	if(session.getAttribute("pag") == null)
		pageContext.forward("index.jsp");
	pageContext.forward((String)session.getAttribute("pag"));
}
%>