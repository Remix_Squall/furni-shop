<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Furni Shop - Bienvenido</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/inicio.css"
	type="text/css" />
</head>
<body>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="contenido">
		<h1>- Bienvenido a Furni Shop -</h1>
		<p>Bienvenidos, esta página web fue diseñada por Luis Romero
			Moreno para la venta de muebles online para un "Proyecto Integrado"
			para el instituto IES Ramón del Valle-Inclán. Espero que les guste.</p>
		<p>En esta tienda ofrecemos un servicio online de venta de
			muebles, en los que los podemos buscar por nombre o categoría.</p>
	</div>
	<div class="footer">Furni Shop © 2014. Creado por Luis Romero
		Moreno para IES Ramón del Valle-Inclán</div>
</body>
</html>