<%@page language="java" contentType="text/html"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.GregorianCalendar"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:useBean id="usuario" scope="session"
	class="furni.shop.beans.Usuario" />
<jsp:setProperty name="usuario" property="*" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Factura Confirmaci�n</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/inicio.css"
	type="text/css" />
</head>
<body>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="contenido">
		<h2>Factura Confirmaci�n</h2>
		<%
			GregorianCalendar fechaActual = dataManager.insertarPedido(usuario,
					(Hashtable) session.getAttribute("cesta"));
				int mes = fechaActual.get(GregorianCalendar.MONTH) + 1;
			if (!(fechaActual == null)) {
				session.removeAttribute("cesta");
		%>
		<p class="info">
			Gracias por su compra.<br /> Su pedido ha sido completado con �xito:
			<%
			out.print(fechaActual.get(GregorianCalendar.DAY_OF_MONTH) 
					+ "/" + mes	+ "/" + fechaActual.get(GregorianCalendar.YEAR) 
					+ " " + fechaActual.get(GregorianCalendar.HOUR_OF_DAY) 
					+ ":" + fechaActual.get(GregorianCalendar.MINUTE) 
					+ ":" + fechaActual.get(GregorianCalendar.SECOND));
		%>
		</p>
		<%
			} else {
		%><p class="error">�Error inesperado procesando el pedido!</p>
		<%
			}
		%>
	</div>
	<div class="footer">Furni Shop � 2014. Creado por Luis Romero
		Moreno para IES Ram�n del Valle-Incl�n</div>
</body>
</html>
