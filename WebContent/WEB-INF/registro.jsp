<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
	<jsp:useBean id="registrado" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
	<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
	<jsp:setProperty name="registrado" property="*" />
	<jsp:setProperty name="usuario" property="*" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Furni Shop - Registro</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/inicio.css"
	type="text/css" />
</head>
<body>
<%
if(dataManager.getLogged(usuario.getId_usuario(), usuario.getContras())){
	pageContext.forward("index.jsp");
}
%>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="registro">
	<h1>Registro de Usuario</h1>
	<%
	if(session.getAttribute("error") != null)
		if((Boolean)session.getAttribute("error")) {
			if((Integer)session.getAttribute("problema") == null)
				session.setAttribute("problema", -1);
			Integer p = (Integer)session.getAttribute("problema");
			switch(p) {
			case 0:
	%>
	<div class="problema">Uno o varios campos están vacíos</div>
	<%
				break;
			case 1:
	%>
	<div class="problema">Los campos Código Postal y Tarjeta de Crédito son de tipo numérico</div>
	<%
				break;
			case 2:
	%>
	<div class="problema">Ya existe un cliente con ese nombre de usuario</div>
	<%
				break;
			default:
				break;
			}
		}
	session.removeAttribute("error");
	%>
		<form method="get">
		<input type="hidden" name="action" value="confirmacion">
		<table>
		<tr><td><h4>Usuario:</h4></td><td><input type="text" name="id_usuario" maxlength="15" value="<%registrado.getId_usuario();%>"></td></tr>
		<tr><td><h4>Password:</h4></td><td><input type="password" name="contras" maxlength="15" value="<%registrado.getContras();%>"></td></tr>
		<tr><td><h4>Nombre:</h4></td><td><input type="text" name="nombre" maxlength="30" value="<%registrado.getNombre();%>"></td></tr>
		<tr><td><h4>Dirección:</h4></td><td><input type="text" name="dir" maxlength="30" value="<%registrado.getDir();%>"></td></tr>
		<tr><td><h4>Ciudad:</h4></td><td><input type="text" name="ciudad" maxlength="20" value="<%registrado.getCiudad();%>"></td></tr>
		<tr><td><h4>Provincia:</h4></td><td><input type="text" name="provincia" maxlength="20" value="<%registrado.getProvincia();%>"></td></tr>
		<tr><td><h4>Código Postal:</h4></td><td><input type="text" name="CP" value="<%registrado.getCP();%>"></td></tr>
		<tr><td><h4>Email:</h4></td><td><input type="text" name="email" maxlength="30" value="<%registrado.getEmail();%>"></td></tr>
		<tr><td><h4>Tarjeta de Crédito:</h4></td><td><input type="text" name="tarjeta" value="<%registrado.getTarjeta();%>"></td></tr>
		</table>
		<input type="submit" value="Confirmación Registro">
		<input type="reset">
		</form>
	</div>
	<div class="footer">Furni Shop © 2014. Creado por Luis Romero
		Moreno para IES Ramón del Valle-Inclán</div>
</body>
</html>