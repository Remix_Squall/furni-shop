<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="error.jsp"%>
<%@ page import="furni.shop.beans.*"%>
<%@ page import="java.util.ArrayList"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Furni Shop - Pedidos Muebles</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/adminis.css"
	type="text/css" />
</head>
<body>
	<%
		String base = (String) application.getAttribute("base");
		if (usuario.getId_usuario() == null) {
			usuario.setId_usuario("");
			usuario.setContras("");
			usuario.setCiudad("");
			usuario.setCP("-1");
			usuario.setDir("");
			usuario.setEmail("");
			usuario.setNombre("");
			usuario.setProvincia("");
			usuario.setTarjeta("-1");
		}
		if (!dataManager.isUserAdmin(usuario.getId_usuario(),
				usuario.getContras())) {
			throw new Exception();
		}
	%>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="contenido">
		<h2>
			Ver pedidos por mueble:
			<%=dataManager.getNombreMueble((request.getParameter("id")))%>
		</h2>
		<table>
			<tr>
				<th>Usuario</th>
				<th>Cantidad</th>
				<th>Subtotal</th>
				<th>Fecha Compra</th>
			</tr>
			<%
				ArrayList<Pedidos> pedidos = dataManager.getPedidosMuebles(request
						.getParameter("id"));
				double total = 0.0;
				for (int i = 0; i < pedidos.size(); i++) {
					int cantidad = Integer.parseInt(pedidos.get(i).getCantidad());
					double precio = Double.parseDouble(dataManager
							.getPrecio((String) request.getParameter("id")));
					double subTotal = Math.rint(precio * cantidad * 100) / 100;
					total += subTotal;
					out.println("<tr><td>" + dataManager.getUsuario(pedidos.get(i))
							+ "</td>" + "<td>" + cantidad + "</td>" + "<td>"
							+ subTotal + "&#8364;</td>" + "<td>"
							+ pedidos.get(i).getFecha() + "</td></tr>");
				}
			%>
			<tr>
				<td colspan="5" id="total" class="total">Total: <%=Math.rint(total * 100) / 100%>&#8364;
				</td>
				<td class="total">&nbsp;</td>
			</tr>
		</table>
	</div>
	<div class="footer">Furni Shop © 2014. Creado por Luis Romero
		Moreno para IES Ramón del Valle-Inclán</div>
</body>
</html>