<%@page language="java" import="java.io.*,java.util.*"
	contentType="text/html"%>
<%
	String base = (String) application.getAttribute("base");
%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />

<%
	dataManager.removeUsuario((String)session.getAttribute("id_usuario"));
	if(session.getAttribute("id_usuario") != null)
		session.setAttribute("exito", true);
	session.removeAttribute("id_usuario");
	pageContext.forward("bajausuario.jsp");
%>