<%@page language="java" import="java.io.*,java.util.*"
	contentType="text/html"%>
<%
	String base = (String) application.getAttribute("base");
%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />

<%
	dataManager.removeCategoria((String)session.getAttribute("id_categoria"));
	if(session.getAttribute("id_categoria") != null)
		session.setAttribute("exito", true);
	session.removeAttribute("id_categoria");
	pageContext.forward("bajaCat.jsp");
%>