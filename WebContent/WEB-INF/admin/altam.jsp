<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="error.jsp"%>
<%@ page import="java.util.Hashtable"%>
<%@ page import="java.util.Enumeration"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<jsp:useBean id="mueble" class="furni.shop.beans.Muebles"
	scope="session"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Furni Shop - Bienvenido</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/adminis.css"
	type="text/css" />
</head>
<body>
	<%
		String base = (String) application.getAttribute("base");
		if (usuario.getId_usuario() == null) {
			usuario.setId_usuario("");
			usuario.setContras("");
			usuario.setCiudad("");
			usuario.setCP("-1");
			usuario.setDir("");
			usuario.setEmail("");
			usuario.setNombre("");
			usuario.setProvincia("");
			usuario.setTarjeta("-1");
		}
		if (!dataManager.isUserAdmin(usuario.getId_usuario(),
				usuario.getContras())) {
			throw new Exception();
		}
	%>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="contenido">
		<h2>Alta Muebles</h2>
		<table>
			<tr>
				<td>
					<form method="post" action="<%=base%>?action=addMueble"
						enctype="multipart/form-data">
						<p>
							Nombre mueble: <input type="text" maxlength="20" name="nombre"
								value="<%=mueble.getNombre()%>" />
						</p>
						<p>
							Categoría: <select name="id_categoria">

								<%
									Hashtable categorias = dataManager.getCategorias();
									Enumeration idsCategorias = categorias.keys();
									while (idsCategorias.hasMoreElements()) {
										Object idCategoria = idsCategorias.nextElement();
										out.println("<option value=\"" + idCategoria + "\">"
												+ categorias.get(idCategoria) + "</option>");
									}
								%>
							</select>
						</p>
						<p>
							Precio: <input type="text" name="precio"
								value="<%=mueble.getPrecio()%>" />
						</p>
						<p>
							Foto: <input type="file" name="foto"
								value="<%=mueble.getFoto()%>"
								accept="image/x-png, image/gif, image/jpeg" />
						</p>
						<p>
							<input type="submit" value="Añadir Mueble">
						</p>
					</form>
				</td>
				<td>
					<%
				if(session.getAttribute("problema") != null) {
					int problema = (Integer)session.getAttribute("problema");
					switch(problema) {
					case 0:
						%>
					<div class="works">Mueble añadido correctamente</div> <%
						break;
					case 1:
						%>
					<div class="error">El fichero que escogió ya existe o no subió el fichero</div> <%
						break;
					case 2:
						%>
					<div class="error">El nombre del mueble no puede estar vacío</div>
					<%
						break;
					case 3:
						%>
					<div class="error">El campo precio no puede estar vacío o
						contener texto</div> <%
						break;
					}
				}
				session.removeAttribute("problema");
				%>
				</td>
			</tr>
		</table>
	</div>
	<div class="footer">Furni Shop © 2014. Creado por Luis Romero
		Moreno para IES Ramón del Valle-Inclán</div>
</body>
</html>