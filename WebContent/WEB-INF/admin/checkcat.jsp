<%@page language="java" import="java.io.*,java.util.*"
	contentType="text/html"%>
<%
	String base = (String) application.getAttribute("base");
%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />

<%
	String categoria = request.getParameter("categoria");
	if(categoria.equals("")) {
		session.setAttribute("problema", 1);
	} else if(dataManager.isCategoria(categoria)) {
		session.setAttribute("problema", 0);
	} else if(!dataManager.isCategoria(categoria)) {
		session.setAttribute("problema", 2);
		dataManager.addCategoria(categoria);
	}
	pageContext.forward("altac.jsp");
%>