<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="error.jsp"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Enumeration"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Furni Shop - Baja Muebles</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/adminis.css"
	type="text/css" />
</head>
<body>
	<%
		String base = (String) application.getAttribute("base");
		if (usuario.getId_usuario() == null) {
			usuario.setId_usuario("");
			usuario.setContras("");
			usuario.setCiudad("");
			usuario.setCP("-1");
			usuario.setDir("");
			usuario.setEmail("");
			usuario.setNombre("");
			usuario.setProvincia("");
			usuario.setTarjeta("-1");
		}
		if (!dataManager.isUserAdmin(usuario.getId_usuario(),
				usuario.getContras())) {
			throw new Exception();
		}
	%>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="contenido">
		<h2>Baja de Muebles sin vender</h2>
		<p>Escoja el mueble que no haya recibido ningún pedido que desee borrar:</p>
		<ul>
		<%
			Hashtable muebles = dataManager.getMueblesSin();
			Enumeration idsMuebles = muebles.keys();
			if(muebles.size() == 0) {
				%>
				<div class="nota">No hay muebles para borrar actualmente</div>
				<%
			}
			while (idsMuebles.hasMoreElements()) {
				Object idMueble = idsMuebles.nextElement();
				out.println("<li><a href=" + base
						+ "?action=checkMB&id="
						+ idMueble.toString() + ">"
						+ muebles.get(idMueble) + "</a></li>");
			}
		%>
		</ul><br>
		<%
			 if (session.getAttribute("exito") != null)
				if((Boolean) session.getAttribute("exito")) {
		%><div class="works">Se ha borrado el mueble correctamente</div>
		<%
				}
			session.removeAttribute("exito");
		%>
	</div>
	<div class="footer">Furni Shop © 2014. Creado por Luis Romero
		Moreno para IES Ramón del Valle-Inclán</div>
</body>
</html>