<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="error.jsp"%>
<%@ page import="furni.shop.beans.Muebles"%>
<%@ page import="furni.shop.beans.Usuario"%>
<%@ page import="java.util.Hashtable"%>
<%@ page import="java.util.Enumeration"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Furni Shop - Bienvenido</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/adminis.css"
	type="text/css" />
</head>
<body>
	<%
		String base = (String) application.getAttribute("base");
		if (usuario.getId_usuario() == null) {
			usuario.setId_usuario("");
			usuario.setContras("");
			usuario.setCiudad("");
			usuario.setCP("-1");
			usuario.setDir("");
			usuario.setEmail("");
			usuario.setNombre("");
			usuario.setProvincia("");
			usuario.setTarjeta("-1");
		}
		if (!dataManager.isUserAdmin(usuario.getId_usuario(),
				usuario.getContras())) {
			throw new Exception();
		}
	%>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="contenido">
		<h2>
			Ver pedidos
			</h2>
			<table>
				<tr>
					<th>Por usuario:</th>
					<th>Por mueble:</th>
				</tr>
				<tr>
					<td><ul>
							<%
								Hashtable usuarios = dataManager.getUsuarios();
								Enumeration idsUsuarios = usuarios.keys();
								while (idsUsuarios.hasMoreElements()) {
									Object idUsuario = idsUsuarios.nextElement();
									out.println("<li><a href=" + base + "?action=pedidoUsuario&id="
											+ idUsuario + ">" + usuarios.get(idUsuario)
											+ "</a></li>");
								}
							%>
						</ul></td>
					<td><ul>
							<%
								Hashtable muebles = dataManager.getMuebles();
								Enumeration idsMuebles = muebles.keys();
								while (idsMuebles.hasMoreElements()) {
									Object idMueble = idsMuebles.nextElement();
									out.println("<li><a href=" + base + "?action=pedidoMueble&id="
											+ idMueble + ">" + muebles.get(idMueble)
											+ "</a></li>");
								}
							%>
						</ul></td>
				</tr>
			</table>
	</div>
	<div class="footer">Furni Shop © 2014. Creado por Luis Romero
		Moreno para IES Ramón del Valle-Inclán</div>
</body>
</html>