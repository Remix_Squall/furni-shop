<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="error.jsp"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Furni Shop - Alta Categorías</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/adminis.css"
	type="text/css" />
</head>
<body>
	<%
		String base = (String) application.getAttribute("base");
		if (usuario.getId_usuario() == null) {
			usuario.setId_usuario("");
			usuario.setContras("");
			usuario.setCiudad("");
			usuario.setCP("-1");
			usuario.setDir("");
			usuario.setEmail("");
			usuario.setNombre("");
			usuario.setProvincia("");
			usuario.setTarjeta("-1");
		}
		if (!dataManager.isUserAdmin(usuario.getId_usuario(),
				usuario.getContras())) {
			throw new Exception();
		}
	%>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="contenido">
		<h2>Alta de Categorías</h2>
		<form method=get">
			<input type="hidden" name="action" value="altaCat" /> <input
				type="text" name="categoria" size="20" /> <input type="submit"
				value="Añadir" />
		</form><br>
		<%
			if (session.getAttribute("problema") != null)
				switch ((Integer) session.getAttribute("problema")) {
				case 0:
		%><div class="error">La categoría ya existe</div>
		<%
			break;
				case 1:
		%><div class="error">No puedes añadir una categoría vacía</div>
		<%
			break;
				case 2:
		%><div class="works">Se ha añadido la categoría correctamente</div>
		<%
			break;
				}
			session.removeAttribute("problema");
		%>
	</div>
	<div class="footer">Furni Shop © 2014. Creado por Luis Romero
		Moreno para IES Ramón del Valle-Inclán</div>
</body>
</html>