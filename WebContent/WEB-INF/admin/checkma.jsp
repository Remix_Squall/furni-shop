<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="error.jsp"%>
<%@ page import="java.util.List"%>
<%@ page import="java.io.*"%>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="org.apache.commons.fileupload.servlet.*"%>
<%@ page import="org.apache.commons.fileupload.disk.*"%>
<%@ page import="org.apache.commons.io.*"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<jsp:useBean id="mueble" class="furni.shop.beans.Muebles"
	scope="session"></jsp:useBean>
<%
	String base = (String) application.getAttribute("base");
	if (usuario.getId_usuario() == null) {
		usuario.setId_usuario("");
		usuario.setContras("");
		usuario.setCiudad("");
		usuario.setCP("-1");
		usuario.setDir("");
		usuario.setEmail("");
		usuario.setNombre("");
		usuario.setProvincia("");
		usuario.setTarjeta("-1");
	}
	if (!dataManager.isUserAdmin(usuario.getId_usuario(),
			usuario.getContras())) {
		throw new Exception();
	}
	FileItemFactory factory = new DiskFileItemFactory();
	ServletFileUpload upload = new ServletFileUpload(factory);

	// req es la HttpServletRequest que recibimos del formulario.
	// Los items obtenidos serán cada uno de los campos del formulario,
	// tanto campos normales como ficheros subidos.
	List items = upload.parseRequest(request);

	// Se recorren todos los items, que son de tipo FileItem
	for (Object item : items) {
		FileItem uploaded = (FileItem) item;

		// Hay que comprobar si es un campo de formulario. Si no lo es, se guarda el fichero
		// subido donde nos interese
		if (!uploaded.isFormField()) {
			// No es campo de formulario, guardamos el fichero en algún sitio
			File fichero = new File(
					"C:\\Users\\Luis1988\\workspace\\Furni Shop 3\\WebContent\\resources\\images",
					uploaded.getName());
			if (!fichero.exists()) {
				mueble.setFoto(uploaded.getName());
				dataManager.insertarMueble(mueble);
				uploaded.write(fichero);
				session.setAttribute("problema", 0);
			}
			else if (fichero.exists()) {
				session.setAttribute("problema", 1);
			}
		} else {
			// es un campo de formulario, podemos obtener clave y valor
			String key = uploaded.getFieldName();
			String valor = uploaded.getString();
			if (key.equals("nombre")) {
				if (valor.equals("")) {
					session.setAttribute("problema", 2);
					break;
				}
				if (!valor.equals(""))
					mueble.setNombre(valor);
			}
			if (key.equals("id_categoria")) {
				mueble.setId_categoria(Integer.parseInt(valor));
			}
			if (key.equals("precio")) {
				try {
					float precio = Float.parseFloat(valor);
					if (precio == 0.0)
						throw new NumberFormatException();
					else
						mueble.setPrecio(precio);
				} catch (NumberFormatException nfe) {
					session.setAttribute("problema", 3);
					break;
				}
			}
		}
	}
	pageContext.forward("altam.jsp");
%>