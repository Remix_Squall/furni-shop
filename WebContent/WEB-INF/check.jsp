<%@page language="java" import="java.io.*,java.util.*"
	contentType="text/html"%>
<%
	String base = (String) application.getAttribute("base");
%>
<jsp:useBean id="usuario" class="furni.shop.beans.Usuario"
	scope="session"></jsp:useBean>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<jsp:setProperty name="usuario" property="*" />

<%
	String USER = usuario.getId_usuario();
	String PASSWORD = usuario.getContras();
	if (!dataManager.getLogged(USER, PASSWORD)) {
		pageContext.include("login.jsp");
		out.println("<div class=\"error\">Usuario/Password Incorrectos</div> "+USER+" "+PASSWORD+" "+dataManager.getLogged(USER, PASSWORD));
	} else {
		pageContext.include("login.jsp");
	}
%>
