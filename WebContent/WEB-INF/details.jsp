<%@page language="java" contentType="text/html"%>
<%@page import="furni.shop.beans.Muebles"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<%
	String base = (String)application.getAttribute("base");
  String imageURL = (String)application.getAttribute("imageURL");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Detalles de Muebles</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/inicio.css" type="text/css" />
</head>
<body>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<div class="contenido">
		<h2>Detalle del Mueble</h2>
		<%
	try {
    String id_mueble = request.getParameter("id_mueble");
    Muebles mueble = dataManager.getDetallesMueble(id_mueble);
    if (mueble != null) {
%>
		<table>
			<tr>
				<td><img src="<%=(imageURL + mueble.getFoto())%>" /></td>
				<td><b><%=mueble.getNombre()%></b><br />
					Precio: <%=mueble.getPrecio()%>&#8364;</td>
			</tr>
			<tr>
				<td colspan="2" align="right"><a class="enlace"
					href="<%=base%>?action=addProducto&id_mueble=<%=mueble.getId_mueble()%>">
						A�adir al Carrito</a></td>
			</tr>
		</table>
		<%
      }
    }
  catch (Exception e) {
	%><p class="error">�Identificador del mueble err�neo!</p>
		<%
    }
  %>
	</div>
	<div class="footer">Furni Shop � 2014. Creado por Luis Romero
		Moreno para IES Ram�n del Valle-Incl�n</div>
</body>
</html>