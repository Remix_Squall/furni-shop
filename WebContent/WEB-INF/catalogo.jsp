<%@page language="java" contentType="text/html"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="furni.shop.beans.Muebles"%>
<jsp:useBean id="dataManager" scope="application"
	class="furni.shop.controller.DataManager" />
<%
	String base = (String)application.getAttribute("base");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Mostrar Cat�logo</title>
<link rel="stylesheet" href="/Furni_Shop_3/resources/css/inicio.css"
	type="text/css" />
</head>
<body>
	<jsp:include page="top.jsp" flush="true" />
	<jsp:include page="left.jsp" flush="true" />
	<%
	String idCategoria = request.getParameter("id");
  String nombreCategoria = null;
  if (idCategoria != null && !idCategoria.trim().equals("")) {
    try {
      nombreCategoria = dataManager.getNombreCategoria(idCategoria);
      }
    catch(NumberFormatException e) {}
    }
  if (nombreCategoria != null) {
%>
	<div class="contenido">
		<h2>Muebles seleccionados</h2>
		<p>
			Categoria: <strong><%=nombreCategoria%></strong>
		</p>
		<table>
			<tr>
				<th>Mueble</th>
				<th>Precio</th>
				<th>Detalles</th>
			</tr>
			<%
	ArrayList muebles = dataManager.getMueblesEnCategoria(idCategoria);
        Iterator iterator = muebles.iterator();
        while (iterator.hasNext()) {
          Muebles mueble = (Muebles)iterator.next();
          int id_mueble = mueble.getId_mueble();
%>
			<tr>
				<td><%=mueble.getNombre()%></td>
				<td><%=mueble.getPrecio()%>&#8364;</td>
				<td><a class="enlace"
					href="<%=base%>?action=detallesMueble&id_mueble=<%=id_mueble%>">
						Detalles</a></td>
			</tr>
			<%
          }
  %>
		</table>
	</div>
	<%
    }
  else {
	%><p class="error">Categor�a err�nea!</p>
	<%
	}
%>
	<div class="footer">Furni Shop � 2014. Creado por Luis Romero
		Moreno para IES Ram�n del Valle-Incl�n</div>
</body>
</html>
