package furni.shop.controller;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import furni.shop.beans.*;

/**
 * Controlador base para todas las operaciones en la tienda
 * de muebles
 * @author Luis Romero Moreno
 * @version v2.0
 */
public class DataManager {
	
	/**
	 * Atributos privados con la URL de la base de datos, 
	 * y su usuario y contraseña. Estos se inicializarán como ""
	 */
	private String dbURL = "";
	private String dbUsuario = "";
	private String dbPassword = "";

	/**
	 * Modifica la URL de la base de datos
	 * @param dbURL URL de la base de datos
	 */
	public void setDbURL(String dbURL) {
		this.dbURL = dbURL;
	}
	
	/**
	 * Devuelve la URL de la base de datos
	 * @return URL de la base de datos
	 */
	public String getDbURL() {
		return dbURL;
	}

	/**
	 * Modifica el usuario de la base de datos
	 * @param dbUserName usuario base de datos
	 */
	public void setDbUsuario(String dbUserName) {
		this.dbUsuario = dbUserName;
	}
	
	/**
	 * Devuelve el usuario de la base de datos
	 * @return usuario base de datos
	 */
	public String getDbUsuario() {
		return dbUsuario;
	}

	/**
	 * Modifica la contraseña de acceso a la base de datos
	 * @param dbPassword contraseña base de datos
	 */
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	
	/**
	 * Devuelve la contraseña con acceso a la base de datos
	 * @return contraseña base de datos
	 */
	public String getDbPassword() {
		return dbPassword;
	}

	/**
	 * Devuelve la conexión de la base de datos a través de
	 * los atributos privados de la clase
	 * @return conexión base de datos
	 */
	public Connection getConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(getDbURL(), getDbUsuario(),
					getDbPassword());
		}
		catch (SQLException e) {
			System.out.println("No se puede conectar a la DB: " + e.getMessage());
		}
		return conn;
	}
	
	/**
	 * Cierra la conexión con la base de datos
	 * @param conn conexión base de datos
	 */
	public void putConnection(Connection conn) {
		if (conn != null) {
			try { conn.close(); }
			catch (SQLException e) { }
		}
	}


	//---------- Operaciones con Categorías ----------
	
	/**
	 * Devuelve el nombre de la categoría según el 
	 * identificador de categoría
	 * @param idCategoria identificador categoría
	 * @return nombre categoría
	 */
	public String getNombreCategoria(String idCategoria) {
		Categoria categoria = CategoriasBD.getCategoriaPorID(this, idCategoria);
		return (categoria == null) ? null : categoria.getCategoria();
	}

	/**
	 * Devuelve una tabla con todas las categorías
	 * existentes
	 * @return Tabla con categorías
	 */
	public Hashtable getCategorias() {
		return CategoriasBD.getTodasCategorias(this);
	}
	
	/**
	 * Devuelve una tabla con las categorías que no tengan asignado
	 * ningún mueble
	 * @return tabla con categorías
	 */
	public Hashtable getCategoriasSin() {
		return CategoriasBD.getCategoriasSinMuebles(this);
	}

	/**
	 * Devuelve un listado con los identificadores de 
	 * todas las categorías
	 * @return lista con identificadores de categorías
	 */
	public Enumeration getIDsCategoria() {
		return CategoriasBD.getTodasCategorias(this).keys();
	}
	
	/**
	 * Devuelve si la categoría con el nombre dado por el
	 * parámetro existe
	 * @param categoria nombre categoría
	 * @return verdadero o falso
	 */
	public boolean isCategoria(String categoria) {
		return CategoriasBD.isCategoria(this, categoria);
	}

	/**
	 * Añade una nueva categoría
	 * @param categoria nombre categoría
	 */
	public void addCategoria(String categoria) {
		Connection connection = getConnection();
		if (connection != null) {
			Statement stmt = null;
			try {
				connection.setAutoCommit(false);
				stmt = connection.createStatement();
				try {
					CategoriasBD.addCategoria(stmt, categoria);
					try { stmt.close(); }
					finally { stmt = null; }
					connection.commit();
				}
				catch (SQLException e) {
					System.out.println("No se puede añadir la categoria: " + e.getMessage());
					try { connection.rollback(); }
					catch (SQLException ee) { }
				}
			}
			catch (SQLException e) {
				System.out.println("No se puede añadir la categoria: " + e.getMessage());
			}
			finally {
				if (stmt != null) {
					try { stmt.close(); }
					catch (SQLException e) { }
				}
				putConnection(connection);
			}
		}
	}
	
	/**
	 * Elimina la categoría por su identificador
	 * @param id_categoria identificador categoría
	 */
	public void removeCategoria(String id_categoria) {
		Connection connection = getConnection();
		if (connection != null) {
			Statement stmt = null;
			try {
				connection.setAutoCommit(false);
				stmt = connection.createStatement();
				try {
					CategoriasBD.removeCategoria(stmt, id_categoria);
					try { stmt.close(); }
					finally { stmt = null; }
					connection.commit();
				}
				catch (SQLException e) {
					System.out.println("No se puede eliminar la categoria: " + e.getMessage());
					try { connection.rollback(); }
					catch (SQLException ee) { }
				}
			}
			catch (SQLException e) {
				System.out.println("No se puede eliminar la categoria: " + e.getMessage());
			}
			finally {
				if (stmt != null) {
					try { stmt.close(); }
					catch (SQLException e) { }
				}
				putConnection(connection);
			}
		}
	}

	//---------- Operaciones con Muebles ----------
	
	/**
	 * Devuelve un listado con todos los muebles que contengan la
	 * palabra clave como parámetro
	 * @param clave palabra clave
	 * @return listado con muebles
	 */
	public ArrayList getResultadosBusqueda(String clave) {
		return MuebleBD.buscarMuebles(this, clave);
	}

	/**
	 * Devuelve un listado con todos los muebles que pertenezcan
	 * al identificador de categoría pasado por parámetro
	 * @param idCategoria identificador categoría
	 * @return lista de muebles
	 */
	public ArrayList getMueblesEnCategoria(String idCategoria) {
		return MuebleBD.getMueblesPorCategoria(this, idCategoria);
	}

	/**
	 * Devuelve un mueble con todos sus detalles que haya sido
	 * seleccionada por su identificador
	 * @param idMueble identificador mueble
	 * @return mueble
	 */
	public Muebles getDetallesMueble(String idMueble) {
		return MuebleBD.getMueblesPorID(this, idMueble);
	}
	
	/**
	 * Devuelve una tabla con los muebles sin haber tenido pedido alguno
	 * @return tabla con el identificador y el nombre del mueble
	 */
	public Hashtable getMueblesSin() {
		return MuebleBD.getMueblesSinPedidos(this);
	}
	
	/**
	 * Devuelve el nombre del mueble por su identificador
	 * @param idMueble identificador mueble
	 * @return nombre mueble
	 */
	public String getNombreMueble(String idMueble) {
		return MuebleBD.getMueblesPorID(this, idMueble).getNombre();
	}
	
	/**
	 * Elimina un mueble según su identificador
	 * @param idMueble identificador mueble
	 */
	public void removeMueble(String idMueble) {
		Connection connection = getConnection();
		if (connection != null) {
			Statement stmt = null;
			try {
				connection.setAutoCommit(false);
				stmt = connection.createStatement();
				try {
					MuebleBD.removeMueble(stmt, idMueble);
					try { stmt.close(); }
					finally { stmt = null; }
					connection.commit();
				}
				catch (SQLException e) {
					System.out.println("No se puede eliminar el mueble: " + e.getMessage());
					try { connection.rollback(); }
					catch (SQLException ee) { }
				}
			}
			catch (SQLException e) {
				System.out.println("No se puede eliminar el mueble: " + e.getMessage());
			}
			finally {
				if (stmt != null) {
					try { stmt.close(); }
					catch (SQLException e) { }
				}
				putConnection(connection);
			}
		}
	}
	
	/**
	 * Inserta un mueble
	 * @param mueble mueble
	 */
	public void insertarMueble(Muebles mueble) {
		Connection connection = getConnection();
		if (connection != null) {
			Statement stmt = null;
			try {
				connection.setAutoCommit(false);
				stmt = connection.createStatement();
				try {
					MuebleBD.addMueble(stmt, mueble);
					try { stmt.close(); }
					finally { stmt = null; }
					connection.commit();
				}
				catch (SQLException e) {
					System.out.println("No se puede añadir el mueble: " + e.getMessage());
					try { connection.rollback(); }
					catch (SQLException ee) { }
				}
			}
			catch (SQLException e) {
				System.out.println("No se puede añadir el mueble: " + e.getMessage());
			}
			finally {
				if (stmt != null) {
					try { stmt.close(); }
					catch (SQLException e) { }
				}
				putConnection(connection);
			}
		}
	}
	
	/**
	 * Devuelve todos los muebles en la base de datos
	 * @return tabla muebles (id_mueble y su nombre)
	 */
	public Hashtable getMuebles() {
		return MuebleBD.getTodosMuebles(this);
	}

	//---------- Operaciones con Usuarios ----------
	
	/**
	 * Devuelve si un usuario está ingresado en la aplicación web
	 * @param idUsuario identificador usuario
	 * @param contras contraseña
	 * @return si está logueado o no el usuario
	 */
	public boolean getLogged(String id_usuario, String contras) {
		return UsuarioBD.IsLogueado(this, id_usuario, contras);
	}

	/**
	 * Devuelve el usuario a partir de su identificador
	 * @param id_usuario identificador usuario
	 * @return usuario
	 */
	public Usuario getUsuario(String id_usuario) {
		return UsuarioBD.getUsuarioDatos(this, id_usuario);
	}

	/**
	 * Devuelve si alguno de los parámetros de usuario está vacío
	 * @param usuario usuario
	 * @return si al menos uno de los parámetros de usuario está vacío
	 */
	public boolean isVacio(Usuario usuario) {
		return usuario.getId_usuario().equals("") ||
				usuario.getCiudad().equals("") ||
				usuario.getContras().equals("") ||
				usuario.getDir().equals("") ||
				usuario.getEmail().equals("") ||
				usuario.getNombre().equals("") ||
				usuario.getProvincia().equals("");
	}

	/**
	 * Devuelve si un usuario ya está registrado según su identificador
	 * @param id_usuario identificador usuario
	 * @return si está registrado un usuario o no
	 */
	public boolean userExists(String id_usuario) {
		return UsuarioBD.userExists(this, id_usuario);
	}

	/**
	 * Da de alta a un nuevo usuario
	 * @param usuario nuevo usuario
	 */
	public void registroUsuario(Usuario usuario) {
		Connection connection = getConnection();
		if (connection != null) {
			Statement stmt = null;
			try {
				connection.setAutoCommit(false);
				stmt = connection.createStatement();
				try {
					UsuarioBD.registroUsuario(stmt, usuario);
					try { stmt.close(); }
					finally { stmt = null; }
					connection.commit();
				}
				catch (SQLException e) {
					System.out.println("No se puede registrar el usuario: " + e.getMessage());
					try { connection.rollback(); }
					catch (SQLException ee) { }
				}
			}
			catch (SQLException e) {
				System.out.println("No se puede registrar el usuario: " + e.getMessage());
			}
			finally {
				if (stmt != null) {
					try { stmt.close(); }
					catch (SQLException e) { }
				}
				putConnection(connection);
			}
		}
	}

	/**
	 * Devuelve si el usuario logueado es administrador o no
	 * @param id_usuario identificador usuario
	 * @param contras contraseña
	 * @return si un usuario es administrador o no
	 */
	public boolean isUserAdmin(String id_usuario, String contras) {
		return UsuarioBD.isUserAdmin(this, id_usuario, contras);
	}
	
	/**
	 * Devuelve una tabla con todos los usuarios
	 * @return tabla con usuarios (id_nombre y nombre)
	 */
	public Hashtable getUsuarios() {
		return UsuarioBD.getTodosUsuarios(this);
	}
	
	/**
	 * Devuelve una tabla con los usuarios que no han realizado
	 * ningún pedido
	 * @return tabla con usuarios (id_usuario y nombre)
	 */
	public Hashtable getUsuariosSin() {
		return UsuarioBD.getUsuariosSinPedidos(this);
	}
	
	/**
	 * Elimina un usuario según su identificador
	 * @param id_usuario identificador usuario
	 */
	public void removeUsuario(String id_usuario) {
		Connection connection = getConnection();
		if (connection != null) {
			Statement stmt = null;
			try {
				connection.setAutoCommit(false);
				stmt = connection.createStatement();
				try {
					UsuarioBD.removeUsuario(stmt, id_usuario);
					try { stmt.close(); }
					finally { stmt = null; }
					connection.commit();
				}
				catch (SQLException e) {
					System.out.println("No se puede eliminar el usuario: " + e.getMessage());
					try { connection.rollback(); }
					catch (SQLException ee) { }
				}
			}
			catch (SQLException e) {
				System.out.println("No se puede eliminar el usuario: " + e.getMessage());
			}
			finally {
				if (stmt != null) {
					try { stmt.close(); }
					catch (SQLException e) { }
				}
				putConnection(connection);
			}
		}
	}

	//---------- Operaciones con Pedidos ----------
	
	/**
	 * Devuelve la fecha en la que se insertó un pedido
	 * @param usuario usuario
	 * @param cesta muebles comprados
	 * @return fecha de la compra
	 */
	public GregorianCalendar insertarPedido(Usuario usuario, Hashtable cesta) {
		GregorianCalendar fechaActual = new GregorianCalendar();
		System.out.println(fechaActual);
		if(UsuarioBD.IsLogueado(this, usuario.getId_usuario(), usuario.getContras())) {
			Connection connection = getConnection();
			if (connection != null) {
				Statement stmt = null;
				try {
					connection.setAutoCommit(false);
					stmt = connection.createStatement();
					try {
						PedidosBD.insertarPedidos(stmt, fechaActual, usuario.getId_usuario(), cesta);
						try { stmt.close(); }
						finally { stmt = null; }
						connection.commit();
					}
					catch (SQLException e) {
						System.out.println("No se puede insertar el pedido: " + e.getMessage());
						try { connection.rollback(); }
						catch (SQLException ee) { }
					}
				}
				catch (SQLException e) {
					System.out.println("No se puede insertar el pedido: " + e.getMessage());
				}
				finally {
					if (stmt != null) {
						try { stmt.close(); }
						catch (SQLException e) { }
					}
					putConnection(connection);
				}
			}
		}
		return fechaActual;
	}
	
	/**
	 * Devuelve un listado con todos los muebles pedidos por un usuario
	 * @param idUsuario identificador usuario
	 * @return lista pedidos con muebles
	 */
	public ArrayList getPedidosUsuarios(String idUsuario) {
		return PedidosBD.getPedidosUsuarios(this, idUsuario);
	}
	
	/**
	 * Devuelve un listado con todos los usuarios que han pedido un mueble
	 * en concreto
	 * @param idMueble identificador mueble
	 * @return lista pedidos con usuarios
	 */
	public ArrayList getPedidosMuebles(String idMueble) {
		return PedidosBD.getPedidosMuebles(this, idMueble);
	}
	
	/**
	 * Devuelve el identificador usuario que realizó el pedido
	 * @param pedido pedido usuario
	 * @return identificador usuario
	 */
	public String getUsuario(Pedidos pedido) {
		return PedidosBD.getUsuario(this, pedido);
	}
	
	/**
	 * Devuelve el precio de un mueble según su identificador
	 * @param idMueble identificador mueble
	 * @return precio mueble
	 */
	public String getPrecio(String idMueble) {
		return PedidosBD.getPrecio(this, idMueble);
	}
	
}