package furni.shop.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import furni.shop.beans.Usuario;

/**
 * Controlador con las operaciones realizadas sobre los usuarios
 * @author Luis Romero Moreno
 * @version v2.0
 */
public class UsuarioBD {

	/**
	 * Devuelve un usuario según su identificador y contraseña
	 * @param dataManager clase controladora superior
	 * @param id_usuario identificador usuario
	 * @param contras contraseña usuario
	 * @return usuario
	 */
	public static Usuario getUsuario(DataManager dataManager, String id_usuario, String contras) {
		Usuario usuario = null;
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select nombre, id_usuario, contras from usuario"
						+ " where UPPER(id_usuario) like UPPER('" + id_usuario + "')"
						+ " and contras like '" + contras + "'";
				try {
					ResultSet rs = s.executeQuery(sql);
					System.out.println(sql);
					while (rs.next()) {
						usuario = new Usuario();
						usuario.setNombre(rs.getString(1));
						usuario.setId_usuario(rs.getString(2));
						usuario.setContras(rs.getString(3));
						System.out.println(rs.getString(1));
					}
					System.out.println("Consulta SQL exitosa");
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Fallo en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return usuario;
	}

	/**
	 * Devuelve si un usuario está logueado en la web
	 * @param dataManager clase controladora superior
	 * @param id_usuario identificador usuario
	 * @param contras contraseña usuario
	 * @return si está logueado un usuario o no
	 */
	public static boolean IsLogueado(DataManager dataManager, String id_usuario, String contras) {
		try{
			return !UsuarioBD.getUsuario(dataManager, id_usuario, contras).getId_usuario().equals("");
		} catch (NullPointerException npe) {
			return false;
		}
	}

	/**
	 * Devuelve los datos de un usuario según su identificador
	 * @param dataManager clase controladora superior
	 * @param id_usuario identificador usuario
	 * @return
	 */
	public static Usuario getUsuarioDatos(DataManager dataManager,
			String id_usuario) {
		Usuario usuario = null;
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select nombre, dir, provincia, ciudad, CP from usuario"
						+ " where UPPER(id_usuario) like UPPER('" + id_usuario + "')";
				try {
					ResultSet rs = s.executeQuery(sql);
					System.out.println(sql);
					while (rs.next()) {
						usuario = new Usuario();
						usuario.setNombre(rs.getString(1));
						usuario.setDir(rs.getString(2));
						usuario.setProvincia(rs.getString(3));
						usuario.setCiudad(rs.getString(4));
						usuario.setCP(rs.getString(5));
						System.out.println(rs.getString(1));
					}
					System.out.println("Consulta SQL exitosa");
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Fallo en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return usuario;
	}

	/**
	 * Devuelve si un usuario existe ya según su identificador
	 * @param dataManager clase controladora superior
	 * @param id_usuario identificador usuario
	 * @return si un usuario existe o no
	 */
	public static boolean userExists(DataManager dataManager, String id_usuario) {
		boolean res = false;
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_usuario from usuario"
						+ " where UPPER(id_usuario) like UPPER('" + id_usuario + "')";
				try {
					ResultSet rs = s.executeQuery(sql);
					System.out.println(sql);
					if(rs.next())
						res = true;
					System.out.println("Consulta SQL exitosa");
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Fallo en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return res;
	}

	/**
	 * Registra un usuario nuevo en la aplicación web
	 * @param stmt atributo usado para la ejecución de una sentencia SQL
	 * @param usuario nuevo usuario
	 * @throws SQLException
	 */
	public static void registroUsuario(Statement stmt, Usuario usuario) 
			throws SQLException {
		String sql = "insert into usuario values ('"
				+ usuario.getId_usuario() + "','" + usuario.getNombre() + "','"
				+ usuario.getDir() + "','" + usuario.getCiudad() + "','"
				+ usuario.getProvincia() + "'," + usuario.getCP() + ","
				+ usuario.getTarjeta() + ",'" + usuario.getEmail() + "','"
				+ usuario.getContras() + "', 0)";
		stmt.executeUpdate(sql);
		System.out.println(sql);		
	}

	/**
	 * Devuelve si un usuario es administrador o no (gestor 1 o 2) según su
	 * identificador y contraseña.
	 * Hay 3 tipos de usuarios: cliente, administrador y superadministrador
	 * @param dataManager clase controladora superior
	 * @param id_usuario identificador usuario
	 * @param contras contraseña usuario
	 * @return si un usuario es administrador
	 */
	public static boolean isUserAdmin(DataManager dataManager,
			String id_usuario, String contras) {
		boolean res = false;
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select gestor from usuario"
						+ " where UPPER(id_usuario) like UPPER('" + id_usuario + "')"
						+ " and contras like '" + contras + "'";
				try {
					ResultSet rs = s.executeQuery(sql);
					System.out.println(sql);
					if(rs.next())
						if(rs.getInt(1) == 1 || rs.getInt(1) == 2)
							res = true;
					System.out.println("Consulta SQL exitosa");
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Fallo en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return res;
	}
	
	/**
	 * Devuelve una tabla con las categorías que no tengan asignado muebles
	 * @param dataManager dataManager atributo controlador superior
	 * @return tabla con categorías
	 */
	public static Hashtable getUsuariosSinPedidos(DataManager dataManager) {
		Hashtable<String, String> usuarios = new Hashtable<String, String>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_usuario, nombre from usuario where "
						+ "not id_usuario in (select id_usuario from pedidos) and "
						+ "not gestor in (2)";
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							usuarios.put(rs.getString(1), rs.getString(2));
						}
					}
					finally { rs.close(); }
				}
				finally {s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Error en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return usuarios;
	}

	/**
	 * Elimina un usuario del sistema según su identificador
	 * @param stmt atributo usado para la ejecución de una sentencia SQL
	 * @param id_usuario identificador de usuario
	 * @throws SQLException
	 */
	public static void removeUsuario(Statement stmt, String id_usuario) throws SQLException {
		String sql = "delete from usuario where id_usuario like '"
				+ id_usuario + "'";
		System.out.println(sql);
		stmt.executeUpdate(sql);
	}

	/**
	 * Devuelve una tabla con todos los usuarios existentes
	 * @param dataManager clase controladora superior
	 * @return tabla usuarios (id_usuario y nombre)
	 */
	public static Hashtable getTodosUsuarios(DataManager dataManager) {
		Hashtable<String, String> usuarios = new Hashtable<String, String>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_usuario, nombre from usuario";
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							usuarios.put(rs.getString(1), rs.getString(2));
						}
					}
					finally { rs.close(); }
				}
				finally {s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Error en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return usuarios;
	}

}
