package furni.shop.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import furni.shop.beans.Muebles;
import furni.shop.beans.Pedidos;

public class PedidosBD {

	/**
	 * Inserta una nueva serie de pedidos de un usuario
	 * @param stmt atributo usado para la ejecución de una sentencia SQL
	 * @param fechaActual fecha Actual
	 * @param id_usuario identificador usuario
	 * @param cesta cesta de compra con todos los muebles y cantidad
	 * @throws SQLException
	 */
	public static void insertarPedidos(Statement stmt, GregorianCalendar fechaActual,
			String id_usuario, Hashtable cesta) throws SQLException {
		String sql;
		Enumeration enumList = cesta.elements();
		int mes = fechaActual.get(GregorianCalendar.MONTH) + 1;
		while (enumList.hasMoreElements()) {
			Pedidos pedido = (Pedidos)enumList.nextElement();
			sql = "insert into pedidos (id_usuario, id_mueble, cantidad,"
					+ " fecha_compra) values ('" + id_usuario + "','"
					+ pedido.getId_mueble() + "','" + pedido.getCantidad() + "',"
					+ " TO_DATE('" + fechaActual.get(GregorianCalendar.DAY_OF_MONTH) 
					+ "-" + mes	+ "-" + fechaActual.get(GregorianCalendar.YEAR) 
					+ " " + fechaActual.get(GregorianCalendar.HOUR_OF_DAY) 
					+ ":" + fechaActual.get(GregorianCalendar.MINUTE) 
					+ ":" + fechaActual.get(GregorianCalendar.SECOND) 
					+ "', 'DD-MM-YYYY HH24:MI:SS'))";
			System.out.println(sql);
			stmt.executeUpdate(sql);
		}
	}

	/**
	 * Devuelve un listado con todos los muebles pedidos por un usuario
	 * @param dataManager clase controladora superior
	 * @param idUsuario identificador usuario
	 * @return lista pedidos con muebles
	 */
	public static ArrayList getPedidosUsuarios(DataManager dataManager,
			String idUsuario) {
		ArrayList<Pedidos> pedidos = new ArrayList<Pedidos>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_mueble, cantidad, fecha_compra from pedidos"
						+ " where UPPER(id_usuario) like UPPER('" + idUsuario + "')";
				System.out.println(sql);
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							Pedidos pedido = new Pedidos();
							pedido.setId_mueble(rs.getString(1));
							pedido.setCantidad(rs.getString(2));
							String fecha = rs.getString(3).replace(".0", "");
							pedido.setFecha(fecha);
							pedidos.add(pedido);
						}
					}
					finally { rs.close(); }
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("No se puede encontrar los pedidos: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return pedidos;
	}

	/**
	 * Devuelve un listado con todos los usuarios que han pedido un mueble
	 * en concreto
	 * @param dataManager clase controladora superior
	 * @param idMueble identificador mueble
	 * @return lista pedidos con usuarios
	 */
	public static ArrayList getPedidosMuebles(DataManager dataManager,
			String idMueble) {
		ArrayList<Pedidos> pedidos = new ArrayList<Pedidos>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_mueble, cantidad, fecha_compra from pedidos"
						+ " where id_mueble = " + idMueble ;
				System.out.println(sql);
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							Pedidos pedido = new Pedidos();
							pedido.setId_mueble(rs.getString(1));
							pedido.setCantidad(rs.getString(2));
							String fecha = rs.getString(3).replace(".0", "");
							pedido.setFecha(fecha);
							pedidos.add(pedido);
						}
					}
					finally { rs.close(); }
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("No se puede encontrar los pedidos: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return pedidos;
	}

	/**
	 * Devuelve el id_usuario que ha hecho el pedido
	 * @param pedido pedido realizado
	 * @return identificador del usuario
	 */
	public static String getUsuario(DataManager dataManager, Pedidos pedido) {
		String id_usuario = "";
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_usuario from pedidos"
						+ " where id_mueble = " + pedido.getId_mueble()
						+ " and fecha_compra in (to_date('" +pedido.getFecha() + 
						"','YYYY-MM-DD HH24:MI:SS'))";
				System.out.println(sql);
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						if (rs.next()) {
							id_usuario = rs.getString(1);
						}
					}
					finally { rs.close(); }
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("No se puede encontrar los pedidos: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return id_usuario;
	}

	public static String getPrecio(DataManager dataManager, String idMueble) {
		String precio = "0.0";
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select precio from muebles"
						+ " where id_mueble = " + idMueble;
				System.out.println(sql);
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						if (rs.next()) {
							precio = rs.getString(1);
						}
					}
					finally { rs.close(); }
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("No se puede encontrar los pedidos: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return precio;
	}

}
