package furni.shop.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import furni.shop.beans.Categoria;

/**
 * Clase controlador que contiene todos los métodos y acciones que se llevarán
 * a cabo con los objetos categorías
 * @author Luis Romero Moreno
 * @version v2.0
 */
public class CategoriasBD {
	
	/**
	 * Método estático que devuelve una categoría según el identificador
	 * recibido de la misma
	 * @param dataManager atributo controlador superior
	 * @param idCategoria identificador categoría
	 * @return objeto Categoria
	 */
	public static Categoria getCategoriaPorID(DataManager dataManager,
			String idCategoria) {
		Categoria categoria = null;
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_categoria, categoria from"
						+ " categoria where id_categoria=" + idCategoria;
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						if (rs.next()) {
							categoria = new Categoria(rs.getInt(1), rs.getString(2));
						}
					}
					finally { rs.close(); }
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Error en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return categoria;
	}

	/**
	 * Devuelve una tabla con todas las categorías existentes
	 * @param dataManager atributo controlador superior
	 * @return tabla con categorías
	 */
	public static Hashtable getTodasCategorias(DataManager dataManager) {
		Hashtable<String, String> categorias = new Hashtable<String, String>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_categoria, categoria from categoria";
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							categorias.put(rs.getString(1), rs.getString(2));
						}
					}
					finally { rs.close(); }
				}
				finally {s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Error en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return categorias;
	}
	
	/**
	 * Devuelve una tabla con las categorías que no tengan asignado muebles
	 * @param dataManager dataManager atributo controlador superior
	 * @return tabla con categorías
	 */
	public static Hashtable getCategoriasSinMuebles(DataManager dataManager) {
		Hashtable<String, String> categorias = new Hashtable<String, String>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_categoria, categoria from categoria where "
						+ "not id_categoria in (select id_categoria from muebles)";
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							categorias.put(rs.getString(1), rs.getString(2));
						}
					}
					finally { rs.close(); }
				}
				finally {s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Error en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return categorias;
	}

	/**
	 * Añade una nueva categoría
	 * @param stmt atributo usado para la ejecución de una sentencia SQL
	 * @param categoria nombre categoría
	 * @throws SQLException
	 */
	public static void addCategoria(Statement stmt, String categoria) throws SQLException {
		String sql = "insert into categoria(categoria) values ('"
				+ categoria + "')";
		stmt.executeUpdate(sql);
		System.out.println(sql);	
	}
	
	/**
	 * Elimina una categoría que no tenga muebles y que no esté repetida
	 * @param stmt atributo usado para la ejecución de una sentencia SQL
	 * @param idCategoria identificador de categoría
	 * @throws SQLException
	 */
	public static void removeCategoria(Statement stmt, String id_categoria) throws SQLException {
		String sql = "delete from categoria where id_categoria = "
				+ id_categoria;
		stmt.executeUpdate(sql);
		System.out.println(sql);	
	}

	/**
	 * Devuelve si existe ya una categoría por su nombre
	 * @param dataManager atributo controlador superior
	 * @param categoria nombre categoría
	 * @return verdadero o falso
	 */
	public static boolean isCategoria(DataManager dataManager, String categoria) {
		boolean res = false;
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_categoria from"
						+ " categoria where UPPER(categoria) like UPPER('" + categoria + "')";
				System.out.println(sql);
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						if (rs.next()) 
							res = true;
					}
					finally { rs.close(); }
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Error en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return res;
	}
	
}
