package furni.shop.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;

import furni.shop.beans.Muebles;
import furni.shop.controller.DataManager;

/**
 * Controlador de operaciones con muebles
 * @author Luis Romero Moreno
 * @version v2.0
 */
public class MuebleBD {

	/**
	 * Lista de muebles por cadena nombre de mueble
	 * @param dataManager clase controladora superior
	 * @param clave cadena que contiene el nombre de un mueble
	 * @return lista de muebles
	 */
	public static ArrayList buscarMuebles(DataManager dataManager,
			String clave) {
		ArrayList<Muebles> muebles = new ArrayList<Muebles>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_mueble, nombre, precio, foto from muebles"
						+ " where UPPER(nombre) like UPPER('%" + clave.trim() + "%')";
				System.out.println(sql);
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							Muebles mueble = new Muebles();
							mueble.setId_mueble(rs.getInt(1));
							mueble.setNombre(rs.getString(2));
							mueble.setPrecio(rs.getFloat(3));
							mueble.setFoto(rs.getString(4));
							muebles.add(mueble);
						}
						System.out.println("Consulta/s realizadas correctamente");
					}
					finally { rs.close(); }
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("No se pueden encontrar muebles:" + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return muebles;
	}

	/**
	 * Lista de muebles por categoría
	 * @param dataManager clase controladora superior
	 * @param idCategoria identificador categoría
	 * @return lista muebles
	 */
	public static ArrayList getMueblesPorCategoria(DataManager dataManager,
			String idCategoria) {
		ArrayList<Muebles> muebles = new ArrayList<Muebles>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_mueble, nombre, precio, foto from muebles"
						+ " where id_categoria=" + idCategoria;
				System.out.println(sql);
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							Muebles mueble = new Muebles();
							mueble.setId_mueble(rs.getInt(1));
							mueble.setNombre(rs.getString(2));
							mueble.setPrecio(rs.getFloat(3));
							mueble.setFoto(rs.getString(4));
							muebles.add(mueble);
						}
					}
					finally { rs.close(); }
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("No se puede encontrar los muebles: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return muebles;
	}

	/**
	 * Devuelve un mueble según su identificador
	 * @param dataManager clase controladora superior
	 * @param id_mueble identificador mueble
	 * @return mueble
	 */
	public static Muebles getMueblesPorID(DataManager dataManager, String id_mueble) {
		Muebles mueble = null;
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_mueble, nombre, precio, foto from muebles"
						+ " where id_mueble=" + id_mueble;
				System.out.println(sql);
				try {
					ResultSet rs = s.executeQuery(sql);
					if (rs.next()) {
						mueble = new Muebles();
						mueble.setId_mueble(rs.getInt(1));
						mueble.setNombre(rs.getString(2));
						mueble.setPrecio(rs.getFloat(3));
						mueble.setFoto(rs.getString(4));
					}
					System.out.println("Consulta SQL exitosa");
				}
				finally { s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Error en la consulta SQL: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return mueble;
	}

	/**
	 * Devuelve una tabla con los muebles que no han recibido ningún pedido
	 * @param dataManager dataManager atributo controlador superior
	 * @return tabla con el identificador y nombre de los muebles
	 */
	public static Hashtable getMueblesSinPedidos(DataManager dataManager) {
		Hashtable<String, String> muebles = new Hashtable<String, String>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_mueble, nombre from muebles where "
						+ "not id_mueble in (select id_mueble from pedidos)";
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							muebles.put(rs.getString(1), rs.getString(2));
						}
					}
					finally { rs.close(); }
				}
				finally {s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Error en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return muebles;
	}

	/**
	 * Elimina el mueble según su identificador
	 * @param stmt atributo usado para la ejecución de una sentencia SQL
	 * @param idMueble identificador mueble
	 * @throws SQLException
	 */
	public static void removeMueble(Statement stmt, String idMueble) throws SQLException {
		String sql = "delete from muebles where id_mueble = "
				+ idMueble;
		stmt.executeUpdate(sql);
		System.out.println(sql);	
	}

	/**
	 * Añade un mueble
	 * @param stmt atributo usado para la ejecución de una sentencia SQL
	 * @param mueble mueble a añadir
	 * @throws SQLException
	 */
	public static void addMueble(Statement stmt, Muebles mueble) throws SQLException {
		String sql = "insert into muebles(nombre,id_categoria,precio,foto) values('"
				+ mueble.getNombre() + "'," + mueble.getId_categoria() + ","
				+ mueble.getPrecio() + ",'" + mueble.getFoto() + "')";
		stmt.executeUpdate(sql);
		System.out.println(sql);		
	}

	/**
	 * Devuelve una tabla con todos los muebles existentes
	 * @param dataManager clase controladora superior
	 * @return tabla muebles con id_mueble y nombre
	 */
	public static Hashtable getTodosMuebles(DataManager dataManager) {
		Hashtable<String, String> muebles = new Hashtable<String, String>();
		Connection connection = dataManager.getConnection();
		if (connection != null) {
			try {
				Statement s = connection.createStatement();
				String sql = "select id_mueble, nombre from muebles";
				try {
					ResultSet rs = s.executeQuery(sql);
					try {
						while (rs.next()) {
							muebles.put(rs.getString(1), rs.getString(2));
						}
					}
					finally { rs.close(); }
				}
				finally {s.close(); }
			}
			catch (SQLException e) {
				System.out.println("Error en la consulta: " + e.getMessage());
			}
			finally {
				dataManager.putConnection(connection);
			}
		}
		return muebles;
	}

}
