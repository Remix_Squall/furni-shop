package furni.shop.beans;

/**
 * Almacena los pedidos realizados por los clientes por mueble y fecha
 * @author Luis Romero Moreno
 * @version v2.0
 */
public class Pedidos {
	
	/**
	 * Atributos privados
	 */
	private String id_mueble, nombre;
	private double precio;
	private String cantidad;
	private String fecha;
	
	/**
	 * Constructor que crea un pedido por los dos parámetros de más abajo
	 * @param mueble todos los datos de un mueble
	 * @param cantidad cantidad de la compra efectuada
	 */
	public Pedidos(Muebles mueble, String cantidad) {
		this.id_mueble = String.valueOf(mueble.getId_mueble());
		this.nombre = mueble.getNombre();
		this.precio = mueble.getPrecio();
		this.cantidad = cantidad;
		setFecha("");
	}

	public Pedidos() {
		id_mueble = "-1";
		nombre = "";
		precio = 0.0;
		cantidad = "0";
		setFecha("");
	}

	/**
	 * Devuelve el número identificador del mueble
	 * @return número identificador del mueble
	 */
	public String getId_mueble() {
		return id_mueble;
	}

	/**
	 * Modifica el número identificador del mueble
	 * @param id_mueble número identificador del mueble
	 */
	public void setId_mueble(String id_mueble) {
		this.id_mueble = id_mueble;
	}

	/**
	 * Devuelve el nombre del mueble
	 * @return nombre del mueble
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Modifica el nombre del mueble
	 * @param nombre nombre del mueble
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Devuelve el precio del mueble
	 * @return precio del mueble
	 */
	public double getPrecio() {
		return precio;
	}

	/**
	 * Modifica el precio del mueble
	 * @param precio precio del mueble
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}

	/**
	 * Devuelve la cantidad pedida de un mueble
	 * @return cantidad de un mueble
	 */
	public String getCantidad() {
		return cantidad;
	}

	/**
	 * Modifica la cantidad pedida de un mueble
	 * @param cantidad cantidad pedidas
	 */
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * Devuelve la fecha de la compra
	 * @return fecha compra
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * Modifica la fecha de la compra
	 * @param fecha fecha compra
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

}
