package furni.shop.beans;

/**
 * Muebles con todos sus datos almacenados
 * @author Luis Romero Moreno
 * @version v2.0
 */
public class Muebles {
	
	/**
	 * Atributos privados
	 */
	private int id_mueble, id_categoria;
	private String nombre, foto;
	private float precio;
	
	/**
	 * Constructor que crea un mueble con los parámetros de más abajo
	 * @param id_mueble número identificador del mueble
	 * @param nombre nombre del mueble
	 * @param id_categoria número identificador de la categoría a la que pertenece
	 * @param precio el valor monetario del mueble
	 * @param foto cadena de caracteres con el nombre del fichero de la foto
	 */
	public Muebles(int id_mueble, String nombre, int id_categoria,
			float precio, String foto) {
		this.id_mueble = id_mueble;
		this.nombre = nombre;
		this.id_categoria = id_categoria;
		this.precio = precio;
		this.foto = foto;
	}

	/**
	 * Constructor vacío de mueble
	 */
	public Muebles() {
		this.id_mueble = -1;
		this.nombre = "";
		this.id_categoria = 1;
		this.precio = 0.0f;
		this.foto = "";
	}

	/**
	 * Devuelve un número identificador del mueble
	 * @return número identificador del mueble
	 */
	public int getId_mueble() {
		return id_mueble;
	}

	/**
	 * Modifica el número identificador de mueble
	 * @param id_mueble número identificador del mueble
	 */
	public void setId_mueble(int id_mueble) {
		this.id_mueble = id_mueble;
	}

	/**
	 * Devuelve el nombre del mueble
	 * @return nombre del mueble
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Modifica el nombre del mueble
	 * @param nombre nombre del mueble
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Devuelve el número identificador de la categoría
	 * @return número identificador de la categoría
	 */
	public int getId_categoria() {
		return id_categoria;
	}

	/**
	 * Modifica el número identificador de la categoría
	 * @param id_categoria identificador de la categoría
	 */
	public void setId_categoria(int id_categoria) {
		this.id_categoria = id_categoria;
	}

	/**
	 * Devuelve el nombre del fichero foto
	 * @return nombre del fichero foto
	 */
	public String getFoto() {
		return foto;
	}

	/**
	 * Modifica el nombre del fichero foto
	 * @param foto nombre del fichero foto
	 */
	public void setFoto(String foto) {
		this.foto = foto;
	}

	/**
	 * Devuelve el valor del mueble
	 * @return valor del mueble
	 */
	public float getPrecio() {
		return precio;
	}

	/**
	 * Modifica el valor del mueble
	 * @param precio valor del mueble
	 */
	public void setPrecio(float precio) {
		this.precio = precio;
	}

}
