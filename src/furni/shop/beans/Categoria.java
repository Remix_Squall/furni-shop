package furni.shop.beans;

/**
 * Categorias que pertenecen los muebles
 * @author Luis Romero Moreno
 * @version v2.0
 */
public class Categoria {
	
	/**
	 * Atributos privados
	 */
	private int id_categoria;
	private String categoria;
	
	/**
	 * Constructor que crea una categoría pasándole dos parámetros
	 * @param id_categoria número identificatorio de la categoría
	 * @param categoria nombre de la categoría
	 */
	public Categoria(int id_categoria, String categoria) {
		this.id_categoria = id_categoria;
		this.categoria = categoria;
	}

	/**
	 * Devuelve un número identificador de la categoría
	 * @return identificador de categoría
	 */
	public int getId_categoria() {
		return id_categoria;
	}

	/**
	 * Modifica el identificador de la categoría
	 * @param id_categoria identificador de la categoría
	 */
	public void setId_categoria(int id_categoria) {
		this.id_categoria = id_categoria;
	}

	/**
	 * Devuelve el nombre de la categoría
	 * @return nombre de la categoría
	 */
	public String getCategoria() {
		return categoria;
	}

	/**
	 * Modifica el nombre de la categoría
	 * @param categoria nombre de la categoría
	 */
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

}
