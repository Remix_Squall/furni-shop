package furni.shop.beans;

/**
 * El usuario es el que realiza la compra o administra la aplicación web
 * dependiendo de sus parámetros
 * @author Luis Romero Moreno
 * @version v2.0
 */
public class Usuario {
	
	/**
	 * Atributos privados
	 */
	private String CP, tarjeta;
	private String id_usuario, nombre, dir, provincia, ciudad, email;
	private String contras;
	
	/**
	 * Constructor que crea un usuario con los parámetros de abajo
	 * @param idUsuario sobrenombre del usuario
	 * @param nombre nombre y apellidos del usuario
	 * @param dir dirección postal del usuario
	 * @param provincia provincia donde reside el usuario
	 * @param ciudad ciudad donde reside el usuario
	 * @param CP código postal
	 * @param tarjeta tarjeta de crédito del usuario
	 * @param email email del usuario
	 * @param contras contraseña del usuario
	 */
	public Usuario(String id_usuario, String nombre, String dir,
			String provincia, String ciudad, String CP, String tarjeta,
			String email, String contras) {
		this.id_usuario = id_usuario;
		this.nombre = nombre;
		this.dir = dir;
		this.provincia = provincia;
		this.ciudad = ciudad;
		this.CP = CP;
		this.tarjeta = tarjeta;
		this.email = email;
		this.contras = contras;
	}

	/**
	 * Constructor vacío
	 */
	public Usuario() {
		this.id_usuario = "";
		this.nombre = "";
		this.dir = "";
		this.provincia = "";
		this.ciudad = "";
		this.CP = "-1";
		this.tarjeta = "-1";
		this.email = "";
		this.contras = "";
	}

	/**
	 * Devuelve el sobrenombre del usuario
	 * @return sobrenombre usuario
	 */
	public String getId_usuario() {
		return id_usuario;
	}

	/**
	 * Modifica el sobrenombre del usuario
	 * @param idUsuario sobrenombre usuario
	 */
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}

	/**
	 * Devuelve el código postal
	 * @return código postal
	 */
	public String getCP() {
		return CP;
	}

	/**
	 * Modifica el código postal
	 * @param cP código postal
	 */
	public void setCP(String cP) {
		CP = cP;
	}

	/**
	 * Devuelve el número de la tarjeta de crédito
	 * @return número tarjeta crédito
	 */
	public String getTarjeta() {
		return tarjeta;
	}

	/**
	 * Modifica el número de la tarjeta de crédito
	 * @param tarjeta número tarjeta crédito
	 */
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * Devuelve el nombre y apellidos del usuario
	 * @return nombre y apellidos
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Modifica el nombre y apellidos del usuario
	 * @param nombre nombre y apellidos
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Devuelve la dirección postal del usuario
	 * @return dirección postal
	 */
	public String getDir() {
		return dir;
	}

	/**
	 * Modifica la dirección postal del usuario
	 * @param dir dirección postal
	 */
	public void setDir(String dir) {
		this.dir = dir;
	}

	/**
	 * Devuelve la provincia donde reside el usuario
	 * @return provincia
	 */
	public String getProvincia() {
		return provincia;
	}

	/**
	 * Modifica la provincia donde reside el usuario
	 * @param provincia provincia
	 */
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	/**
	 * Devuelve la población donde reside el usuario
	 * @return población
	 */
	public String getCiudad() {
		return ciudad;
	}

	/**
	 * Modifica la población donde reside el usuario
	 * @param ciudad población
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	/**
	 * Devuelve la dirección de correo eléctrónico
	 * @return dirección correo electrónico
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Modifica la dirección de correo electrónico
	 * @param email dirección correo electrónico
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Devuelve la contraseña del usuario
	 * @return contraseña
	 */
	public String getContras() {
		return contras;
	}

	/**
	 * Modifica la contraseña del usuario
	 * @param contras contraseña
	 */
	public void setContras(String contras) {
		this.contras = contras;
	}

}
