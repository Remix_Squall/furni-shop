package furni.shop;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import furni.shop.controller.DataManager;

/**
 * Servlet implementation class ShopServlet
 */
@WebServlet(
		urlPatterns = { "/furnishop/*" }, 
		initParams = { 
				@WebInitParam(name = "base", value = "/Furni_Shop_3/furnishop"), 
				@WebInitParam(name = "imageURL", value = "/Furni_Shop_3/resources/images/"),
				@WebInitParam(name = "jdbcDriver", value = "oracle.jdbc.driver.OracleDriver"), 
				@WebInitParam(name = "dbURL", value = "jdbc:oracle:thin:@localhost:1521:luis1988"),
				@WebInitParam(name = "dbUserName", value = "sys as sysdba"),
				@WebInitParam(name = "dbPassword", value = "sandrilla")
		})

/**
 * Clase principal que ejecutará la vista de la aplicación e inicializará los valores
 * de la misma para la ejecución del controlador
 * @author Luis Romero Moreno
 * @version v2.0
 */
public class FurniShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FurniShopServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void init(ServletConfig config) throws ServletException {
		System.out.println("*** Iniciando servlet ***");
		super.init(config);

		DataManager dataManager = new DataManager();
		dataManager.setDbURL(config.getInitParameter("dbURL"));
		dataManager.setDbUsuario(config.getInitParameter("dbUserName"));
		dataManager.setDbPassword(config.getInitParameter("dbPassword"));

		ServletContext context = config.getServletContext();
		context.setAttribute("base", config.getInitParameter("base"));
		context.setAttribute("imageURL", config.getInitParameter("imageURL"));
		context.setAttribute("dataManager", dataManager);

		try {  // load the database JDBC driver

			Class.forName(config.getInitParameter("jdbcDriver"));
		}
		catch (ClassNotFoundException e) {
			System.out.println(e.toString());
		}
	}

	/**
	 * Recoge las peticiones y las publica en el Servlet
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * Ejecuta las acciones recogidas dentro del Servlet
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String base = "/WEB-INF/";
		String url = base + "index.jsp";
		String action = request.getParameter("action");
		if (action != null) {
			if (action.equals("buscar"))
				url = base + "buscador.jsp";
			else if (action.equals("login")) 
				url = base + "index.jsp";
			else if (action.equals("seleccionarCatalogo"))
				url = base + "catalogo.jsp";
			else if (action.equals("detallesMueble")) 
				url = base + "details.jsp";
			else if (action.matches("(mostrarCarrito|(add|actualizar|borrar)Producto)"))
				url = base + "cesta.jsp";
			else if (action.equals("comprar") || action.equals("login2"))
				url = base + "compra.jsp";
			else if (action.equals("confirmarPedido"))
				url = base + "confirmacion.jsp";
			else if (action.equals("cerrarSesion"))
				url = base + "cierre.jsp";
			else if (action.equals("registro"))
				url = base + "registro.jsp";
			else if (action.equals("confirmacion"))
				url = base + "confirmacionRegistro.jsp";
			else if (action.equals("admin"))
				url = base + "admin/index.jsp";
			else if (action.equals("altaM"))
				url = base + "admin/altam.jsp";
			else if (action.equals("bajaM"))
				url = base + "admin/bajam.jsp";
			else if (action.equals("altaCat"))
				url = base + "admin/altaCat.jsp";
			else if (action.equals("bajaCat"))
				url = base + "admin/bajaCat.jsp";
			else if (action.equals("checkCatA"))
				url = base + "admin/checkcata.jsp";
			else if (action.equals("checkCatB"))
				url = base + "admin/checkcatb.jsp";
			else if (action.equals("borraCat"))
				url = base + "admin/borracat.jsp";
			else if (action.equals("bajaU"))
				url = base + "admin/bajausuario.jsp";
			else if (action.equals("checkUsuB"))
				url = base + "admin/checkusub.jsp";
			else if (action.equals("borraUsu"))
				url = base + "admin/borrausu.jsp";
			else if (action.equals("checkMB"))
				url = base + "admin/checkmb.jsp";
			else if (action.equals("borraM"))
				url = base + "admin/borram.jsp";
			else if (action.equals("addMueble"))
				url = base + "admin/checkma.jsp";
			else if (action.equals("verPedidos"))
				url = base + "admin/pedidos.jsp";
			else if (action.equals("pedidoMueble"))
				url = base + "admin/muebles.jsp";
			else if (action.equals("pedidoUsuario"))
				url = base + "admin/usuarios.jsp";
		}
		RequestDispatcher requestDispatcher =
				getServletContext().getRequestDispatcher(url);
		requestDispatcher.forward(request, response);
	}
}